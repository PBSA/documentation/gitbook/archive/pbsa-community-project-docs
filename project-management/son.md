# SON

## SON Sprint Apr13-Apr 24 <a href="#gpos-sprint-jan-2-jan16" id="gpos-sprint-jan-2-jan16"></a>

![](../.gitbook/assets/SON\_BURNDOWN\_APR22.jpg)

​

![](../.gitbook/assets/SON\_ISSUES\_APR22.jpg)





### QA Report <a href="#qa-report" id="qa-report"></a>

Provide QA data, Zephyr charts

### Sprint Deliverables <a href="#sprint-deliverables" id="sprint-deliverables"></a>

* [SON206 - Plugin SON Heartbeat changes](https://github.com/peerplays-network/peerplays/pull/250)
* [ SON194-SON195 - Report SON Down, addition of SON Account for sidechain consensus #244](https://github.com/peerplays-network/peerplays/pull/244)

​
