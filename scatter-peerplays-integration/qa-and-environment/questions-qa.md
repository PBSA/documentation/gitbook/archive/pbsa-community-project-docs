---
description: QA inputs/questions/comments
---

# Questions (QA)

1. Does QA team require to run the testing scenarios for Network (Blockchain) connectivity as per these requirements:&#x20;

![](<../../.gitbook/assets/image (101).png>)

If yes the requirements (details) are missing and should be covered.

2\. For this section we need to follow and reference to the required login convention as per this guide: [https://github.com/peerplays-network/peerplays/wiki/Account-Names](https://github.com/peerplays-network/peerplays/wiki/Account-Names). Please reference to this page.

![](<../../.gitbook/assets/image (108).png>)

3\. Shouldn't it follows that convention: [https://github.com/peerplays-network/peerplays/wiki/Account-Names](https://github.com/peerplays-network/peerplays/wiki/Account-Names)? I am confused

![](<../../.gitbook/assets/image (12).png>)

4\. Why do we use the Import definition for the Login process? It's confusing. **Note:** there is no definition for Import (Login) in other existing DaPP applications I believe: Bookie,  Wallet, 5050. So don't see any reason to use Import abbreviation for Account's Login process.

&#x20;

![](<../../.gitbook/assets/image (97).png>)

**5.** Shouldn't it follows that convention: [https://github.com/peerplays-network/peerplays/wiki/Account-Names](https://github.com/peerplays-network/peerplays/wiki/Account-Names)? I am confused

![](<../../.gitbook/assets/image (9).png>)

6\. The error handling coverage for **3.6 Send PPY** section is missing. We could follow the existing one in the wallet (Send option):&#x20;

![](<../../.gitbook/assets/image (48).png>)

Same will be relevant for not enough balance to send the amount. We need to support the error handling for such cases as well

7\. Some details for QR code scanning are needed (e.g. expected actual results, the way to use it).

![](<../../.gitbook/assets/image (105).png>)

8\. There is the different flow between the new Scatter wallet account creation a the existing one. Do we need to test both flows (I believe that yes)? What are the special points/scenarios should be covered by QA team? If the information is missing in the current requirements it should be added.  &#x20;
