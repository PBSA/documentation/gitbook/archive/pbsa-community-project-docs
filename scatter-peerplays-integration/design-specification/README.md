---
description: Contains Low-level and High-level specs
---

# Design Specification

{% content-ref url="low-level.md" %}
[low-level.md](low-level.md)
{% endcontent-ref %}

{% content-ref url="high-level.md" %}
[high-level.md](high-level.md)
{% endcontent-ref %}

